#pragma once
#include "Resourses.h"
#pragma pack(push, VK_PACK)
#include "iterator.h"

#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif

//////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~!!DECLARATION!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//////////////////////////////////////////////////////////////////////////
VK_NAMESPACE_BEG
template<class _Ty>
class Node {
public:
	Node	*next;
	Node	*prev;
	_Ty		data;

	Node(Node *next, Node *prev, const _Ty &data) :
		next(next), prev(prev), data(data) {}
	~Node()							= default;
	Node()							= delete;
	Node(const Node&)				= delete;
	Node(const Node&&)				= delete;
	Node& operator=(const Node&)	= delete;
	Node& operator=(const Node&&)	= delete;
};





template<class _Ty>
class List{
public:
//methods' type: TYPE-MEMBERS
	unsigned const	size_t = sizeof(_Ty);
	using			type = Node<_Ty>;
private:
	type			*head;
	type			*end;
	unsigned		size;

	void CreateTheFirstElement(const _Ty & value);
public:




	//~~~~~~~~~~ITERATORS~~~~~~~~~~//
private:
	class Iterator : public BaseIterator<_Ty> {
	protected:
		type		*curr;
		Iterator(type *curr);
		friend class List<_Ty>;
	public:
		Iterator				();
		operator _Ty		() override;
		~Iterator				() = default;
	};
public:
	class InputIterator : public Iterator{
	public:
		InputIterator			() = default;
		InputIterator			(Iterator);
		const _Ty*	operator->	()const;
		const _Ty&	operator*	()const;
		Iterator&	operator++	();
		Iterator&	operator++	(int);
		bool		operator!=	(const Iterator&);
		bool		operator==	(const Iterator&);
		Iterator&	operator=	(const Iterator&);
		~InputIterator			() = default;
	};
	class OutputIterator : public Iterator {
	public:
		OutputIterator			() = default;
		OutputIterator			(Iterator);
		_Ty&		operator*	();
		const _Ty&	operator*	()const;
		Iterator&	operator++	();
		Iterator&	operator++	(int);
		Iterator&	operator=	(const Iterator&);
		~OutputIterator			() = default;
	};
	class ForwardIterator : public Iterator {
	public:
		ForwardIterator			() = default;
		ForwardIterator			(Iterator);
		_Ty*		operator->	();
		const _Ty*	operator->	() const;
		_Ty&		operator*	();
		const _Ty&	operator*	() const;
		Iterator&	operator++	();
		Iterator&	operator++	(int);
		Iterator&	operator=	(const Iterator&);
		bool		operator!=	(const Iterator&);
		bool		operator==	(const Iterator&);
		~ForwardIterator		() = default;
	};
	class BidirectionalIterator : public Iterator {
	public:
		BidirectionalIterator	() = default;
		BidirectionalIterator	(Iterator);
		_Ty&					operator*();
		const _Ty&	operator*	() const;
		Iterator&	operator++	();
		Iterator&	operator++	(int);
		Iterator&	operator--	();
		Iterator&	operator--	(int);
		Iterator&	operator=	(const Iterator&);
		bool		operator!=	(const Iterator&);
		bool		operator==	(const Iterator&);
		~BidirectionalIterator	() = default;
	};
	class RandomAccessIterator : public Iterator {
	public:
		RandomAccessIterator	() = default;
		RandomAccessIterator	(Iterator);
		_Ty&		operator[]	(int i);
		const _Ty&	operator[]	(int i) const;
		_Ty*		operator->	();
		const _Ty*	operator->	() const;
		_Ty&		operator*	();
		const _Ty&	operator*	() const;
		Iterator&	operator++	();
		Iterator&	operator++	(int);
		Iterator&	operator--	();
		Iterator&	operator--	(int);
		Iterator&	operator+=	(int);
		Iterator&	operator-=	(int);
		Iterator	operator+	(int);
		Iterator	operator-	(int);
		Iterator&	operator=	(const Iterator&);
		bool		operator!=	(const Iterator&);
		bool		operator==	(const Iterator&);
		~RandomAccessIterator	() = default;
	};


	//~~~~~~~~~~METHODS~~~~~~~~~~//
//access to elements
	_Ty&			GetFront		();
	_Ty&			GetBack			();
	Iterator		GetBegin		();
	const Iterator	GetCBegin		()const;
	Iterator		GetEnd			();
	const Iterator	GetCEnd			()const;

//methods' type: CAPACITY
	bool			IsEmpty			();
	unsigned		GetSize			() const;

//methods' type: CAPACITY
	void			Clear			();
	void			PushBack		(const _Ty& value);
	void			PushFront		(const _Ty& value);
	void			EmplaceBack		(const _Ty& value);
	void			EmplaceFront	(const _Ty& value);
	void			PopBack			();
	void			PopFront		();

//methods' type: GENERALL
	List();
	~List();
	List(const List&)				= delete;
	List(const List&&)				= delete;
	List& operator=(const List&)	= delete;
	List& operator=(const List&&)	= delete;
};











//////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~!!IMPLEMENTATION!!~~~~~~~~~~~~~~~~~~~~~~~~~~//
//////////////////////////////////////////////////////////////////////////

//~~~~~~~~~~~~~~ITERATORS~~~~~~~~~~~~~~//
template<class _Ty>
List<_Ty>::Iterator::operator _Ty(){
	if (this->curr == nullptr)throw "Elements not found";
	return this->curr->data;
}
template<class _Ty>
List<_Ty>::Iterator::Iterator(type * curr) :
	curr(curr){}

template<class _Ty>
List<_Ty>::Iterator::Iterator(){
	this->curr = nullptr;
}
template<class _Ty>
typename List<_Ty>::Iterator List<_Ty>::GetBegin(){
	return Iterator(this->head);
}
template<class _Ty>
typename const List<_Ty>::Iterator List<_Ty>::GetCBegin() const{
	return Iterator(this->head);
}
template<class _Ty>
typename List<_Ty>::Iterator List<_Ty>::GetEnd() {
	return Iterator((this->end)->next);
}
template<class _Ty>
typename const List<_Ty>::Iterator List<_Ty>::GetCEnd() const {
	return Iterator(this->end);
}

//RANDOM-ACCESS ITERATOR
template<class _Ty>
List<_Ty>::RandomAccessIterator::RandomAccessIterator(Iterator it) {
	this->curr = it.curr;
}
template<class _Ty>
const _Ty & List<_Ty>::RandomAccessIterator::operator[](int pos) const {
	type * p = this->curr;
	for (int i = 0; i < pos; i++)
		if (p == nullptr)
			throw std::exception("Read access violation by the iterator's adress");
		else
			p = p->next;

	return p->data;
}
template<class _Ty>
_Ty & List<_Ty>::RandomAccessIterator::operator[](int pos) {
	type * p = this->curr;
	for (int i = 0; i < pos; i++)
		if (p == nullptr)
			throw std::exception("Read access violation by the iterator's adress");
		else
			p = p->next;

	return p->data;
}
template<class _Ty>
const _Ty * List<_Ty>::RandomAccessIterator::operator->() const {
	return &(this->curr->data);
}
template<class _Ty>
_Ty * List<_Ty>::RandomAccessIterator::operator->() {
	return &(this->curr->data);
}
template<class _Ty>
_Ty & List<_Ty>::RandomAccessIterator::operator*() {
	return this->curr->data;
}
template<class _Ty>
const _Ty & List<_Ty>::RandomAccessIterator::operator*() const {
	return this->curr->data;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::RandomAccessIterator::operator++() {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::RandomAccessIterator::operator++(int) {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::RandomAccessIterator::operator--() {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->prev;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::RandomAccessIterator::operator--(int) {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->prev;
	return *this;
}
template<class _Ty>
bool List<_Ty>::RandomAccessIterator::operator!=(const Iterator &other) {
	return this->curr != other.curr;
}
template<class _Ty>
bool List<_Ty>::RandomAccessIterator::operator==(const Iterator &other) {
	return this->curr == other.curr;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::RandomAccessIterator::operator=(const Iterator &it) {
	this->curr = it.curr;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::RandomAccessIterator::operator+=(int pos){
	if(pos >= 0)
		for (int i = 0; i < pos; i++)
			(*this)++;
	else {
		pos *= -1;
		for (int i = 0; i < pos; i++)
			(*this)--;
	}
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::RandomAccessIterator::operator-=(int pos) {
	if (pos >= 0)
		for (int i = 0; i < pos; i++)
			(*this)--;
	else {
		pos *= -1;
		for (int i = 0; i < pos; i++)
			(*this)++;
	}
	return *this;
}

//BIDIRECTIONAL ITERATOR
template<class _Ty>
List<_Ty>::BidirectionalIterator::BidirectionalIterator(Iterator it) {
	this->curr = it.curr;
}
template<class _Ty>
_Ty & List<_Ty>::BidirectionalIterator::operator*() {
	return this->curr->data;
}
template<class _Ty>
const _Ty & List<_Ty>::BidirectionalIterator::operator*() const {
	return this->curr->data;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::BidirectionalIterator::operator++() {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::BidirectionalIterator::operator++(int) {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::BidirectionalIterator::operator--() {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->prev;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::BidirectionalIterator::operator--(int) {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->prev;
	return *this;
}
template<class _Ty>
bool List<_Ty>::BidirectionalIterator::operator!=(const Iterator &other) {
	return this->curr != other.curr;
}
template<class _Ty>
bool List<_Ty>::BidirectionalIterator::operator==(const Iterator &other) {
	return this->curr == other.curr;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::BidirectionalIterator::operator=(const Iterator &it) {
	this->curr = it.curr;
	return *this;
}


//FORWARD ITERATOR
template<class _Ty>
List<_Ty>::ForwardIterator::ForwardIterator(Iterator it) {
	this->curr = it.curr;
}
template<class _Ty>
const _Ty * List<_Ty>::ForwardIterator::operator->() const {
	return &(this->curr->data);
}
template<class _Ty>
_Ty * List<_Ty>::ForwardIterator::operator->(){
	return &(this->curr->data);
}
template<class _Ty>
_Ty & List<_Ty>::ForwardIterator::operator*(){
	return this->curr->data;
}
template<class _Ty>
const _Ty & List<_Ty>::ForwardIterator::operator*() const {
	return this->curr->data;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::ForwardIterator::operator++() {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::ForwardIterator::operator++(int) {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
bool List<_Ty>::ForwardIterator::operator!=(const Iterator &other) {
	return this->curr != other.curr;
}
template<class _Ty>
bool List<_Ty>::ForwardIterator::operator==(const Iterator &other) {
	return this->curr == other.curr;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::ForwardIterator::operator=(const Iterator &it) {
	this->curr = it.curr;
	return *this;
}

//INPUT ITERATOR
template<class _Ty>
List<_Ty>::InputIterator::InputIterator(Iterator it) {
	this->curr = it.curr;
}
template<class _Ty>
const _Ty * List<_Ty>::InputIterator::operator->()const {
	return &(this->curr->data);
}
template<class _Ty>
const _Ty & List<_Ty>::InputIterator::operator*()const {
	return this->curr->data;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::InputIterator::operator++(){
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::InputIterator::operator++(int){
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
bool List<_Ty>::InputIterator::operator!=(const Iterator &other){
	return this->curr != other.curr;
}
template<class _Ty>
bool List<_Ty>::InputIterator::operator==(const Iterator &other) {
	return this->curr == other.curr;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::InputIterator::operator=(const Iterator &it){
	this->curr = it.curr;
	return *this;
}

//OUTPUT ITERATOR
template<class _Ty>
List<_Ty>::OutputIterator::OutputIterator(Iterator it){
	this->curr = it.curr;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::OutputIterator::operator++(){
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::OutputIterator::operator++(int) {
	if (this->curr == nullptr)
		throw std::exception("Read access violation by the iterator's adress");
	this->curr = this->curr->next;
	return *this;
}
template<class _Ty>
_Ty & List<_Ty>::OutputIterator::operator*(){
	return this->curr->data;
}
template<class _Ty>
typename List<_Ty>::Iterator & List<_Ty>::OutputIterator::operator=(const Iterator &it){
	this->curr = it.curr;
	return *this;
}
template<class _Ty>
const _Ty & List<_Ty>::OutputIterator::operator*() const{
	return this->curr->data;
}






//~~~~~~~~~~~~~~LIST~~~~~~~~~~~~~~//
template<class _Ty>
_Ty & List<_Ty>::GetFront(){
	return head->data;
}

template<class _Ty>
_Ty & List<_Ty>::GetBack() {
	return end->data;
}

template<class _Ty>
List<_Ty>::List(){
	end		= nullptr;
	head	= nullptr;
	size	= 0;
}

template<class _Ty>
List<_Ty>::~List(){
	Clear();
}

template<class _Ty>
void List<_Ty>::CreateTheFirstElement(const _Ty & value){
	type *fake_node = new type(nullptr, nullptr, value);
	head = end = fake_node;
	size++;
}

template<class _Ty>
bool List<_Ty>::IsEmpty(){
	return (size == 0) ? true : false;
}

template<class _Ty>
unsigned List<_Ty>::GetSize() const{
	return size;
}

template<class _Ty>
void List<_Ty>::Clear(){
	type* fake_node = this->head;
	while (fake_node != nullptr) {
		fake_node = head->next;
		delete this->head;
		head = fake_node;
	}
	size = 0;
	end = head = nullptr;
}

template<class _Ty>
void List<_Ty>::PushBack(const _Ty & value){
	if (head == nullptr || end == nullptr) {
		CreateTheFirstElement(value);
		return;
	}
	type *fake_node = new type(nullptr, end, value);
	end->next = fake_node;
	end = fake_node;
	size++;
}

template<class _Ty>
void List<_Ty>::PushFront(const _Ty & value) {
	if (head == nullptr || end == nullptr) {
		CreateTheFirstElement(value);
		return;
	}
	type *fake_node = new type(head, nullptr, value);
	head->prev = fake_node;
	head = fake_node;
	size++;
}

template<class _Ty>
void List<_Ty>::EmplaceBack(const _Ty & value) {
	if (head == nullptr || end == nullptr) {
		CreateTheFirstElement(value);
		return;
	}
	type *fake_node = new type(nullptr, end->prev, value);
	(end->prev)->next = fake_node;
	delete end;
	end = fake_node;
}

template<class _Ty>
void List<_Ty>::EmplaceFront(const _Ty & value) {
	if (head == nullptr || end == nullptr) {
		CreateTheFirstElement(value);
		return;
	}
	type *fake_node = new type(head->next, nullptr, value);
	(head->next)->prev = fake_node;
	delete head;
	head = fake_node;
}

template<class _Ty>
void List<_Ty>::PopBack() {
	if (head == nullptr || end == nullptr) std::exception("No the nodes");
	type *fake_pointer = end->prev;
	(end->prev)->next = nullptr;
	delete end;
	end = fake_pointer;
	size--;
}

template<class _Ty>
void List<_Ty>::PopFront() {
	if (head == nullptr || end == nullptr) std::exception("No the nodes");
	type *fake_pointer = head->next;
	(head->next)->prev = nullptr;
	delete head;
	head = fake_pointer;
	size--;
}

VK_NAMESPACE_END
#pragma pack(pop)